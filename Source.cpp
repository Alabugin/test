#include<map>
#include<vector>
#include<iostream>
#include<fstream>
#include<string>
using namespace std;
map < char, int > Alphabet1 ;
vector<char> Alphabet2;

void make_alphabet(){
	char symbol = 'a';
	for (int i = 0; i < 26; i++){
		Alphabet2.push_back(symbol);
		Alphabet1.insert(pair<char,int>(symbol, i));
		symbol ++;
	}
}
void get_text(string file_name,vector<int> &text ){
	fstream file(file_name);
	char temp;
	while (!file.eof()){
		file >> temp;
		if ((temp >= 65 && temp <= 90) || (temp >= 97 && temp <= 122)){
			temp = tolower(temp);
			text.push_back(Alphabet1[temp]);
		}
	}
}
void encrypte(string output_file, vector<int> text, int key){
	fstream file(output_file);
	int temp = 0;
	for each (int x in text){
		temp = (x + key) % 26;
		file << Alphabet2[temp];
	}
}
void decrypte(string input_file, string output_file, int key){
	fstream file1(input_file);
	fstream file2(output_file);
	vector<int> text;
	char temp;
	while (!file1.eof()){
		file1 >> temp;
		if (file1.eof())break;
		file2 << Alphabet2[Alphabet1[temp] - key];
	}

}
void brutforce(string input_file, string output_file){
	fstream file1(input_file);
	fstream file2(output_file);
	vector<int> text;
	char temp;
	while (!file1.eof()){
		file1 >> temp;
		if (file1.eof())break;
		text.push_back(Alphabet1[temp]);
	}
	for (int key = 0; key < 26; key++){
		cout << key << ":";
		for (int i = 0; i < 15 && i < text.size(); i++){
			int temp = (text[i] - key);
			if (temp < 0)
				temp = 26 + temp;
			cout << Alphabet2[temp];
		}
		cout << endl<<endl;
	}
	cout << endl << "Which text is correct?";
	int correct_key = 0;
	cin >> correct_key;
	for each (int x in text){
		temp = (x - correct_key)%26;
		if (temp < 0)
			temp = 26 + temp;
		file2 << Alphabet2[temp];
	}
}

int main(){
	int key = 0;
	make_alphabet();
	vector<int>text;
	get_text("input.txt", text);
	cout << "Please, enter the key for encryption(0..25):\n";
	cin >> key;
	key = key % 26;
	encrypte("encrypted_text.txt", text, key);
	/*
	cout << "Please, enter the key for decryption(0..25):\n";
	int decryption_key = 0;
	cin >> decryption_key;
	decrypte("encrypted_text.txt", "decrypted_text.txt", decryption_key);

	brutforce("encrypted_text.txt", "hacked_text.txt");
	*/
}
